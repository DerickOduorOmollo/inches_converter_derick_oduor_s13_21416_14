package com.derickoduor.androidconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button convert,exit;
    EditText millimeters,inches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        convert=(Button)findViewById(R.id.convert);
        exit=(Button)findViewById(R.id.exit);

        millimeters=(EditText)findViewById(R.id.millimeters);
        inches=(EditText)findViewById(R.id.inches);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inches.setText(Double.toString(Double.parseDouble(millimeters.getText().toString())/25.4));
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
